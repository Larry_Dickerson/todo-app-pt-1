import React from "react"

export default function TodoItem (props) {
 
    return (
        <li className={props.todo.completed ? "completed" : ""}>
          <div className="view">
            <input className="toggle" 
            type="checkbox"  checked={props.todo.completed}
            onChange={() => props.handleChange(props.todo.id)}/>
            <label>{props.todo.title}</label>
            <button className="destroy" onClick={() => props.delete(props.todo.id)} />
          </div>
        </li>
      );
}