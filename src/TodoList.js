import React, { Component } from "react";
import TodoItem from "./TodoItem"
import todosData from "./todos.json";


export default class TodoList extends Component {
    state = {
        todos: todosData,
        input: '',
        itemCount: 5,
        newItem: ""
    };
// stack overflow, "How do I add input data to JSON array in React, https://stackoverflow.com/questions/51699285/how-do-i-add-input-data-to-json-array-in-react "
    addItem = event => {
        event.preventDefault()
        this.setState(prevState => {
            return {
                todos: [
                    ...prevState.todos,
                    { id: Math.random(), title: this.state.input, completed: false }
                ],
                newItem: this.state.input,
                input: '',
                itemCount: prevState.itemCount + 1
            }
        })
        console.log(this.state.input)
    }

    handleInput = (event) => {
        this.setState({input: event.target.value})
} 


    // Bob Ziroll, Scrimba, Introduction to React: "React todo app 6.1"
    handleChange = (id) => {
        this.setState(prevState => {
            const newTodos = prevState.todos.map(todo => {
                if (todo.id === id) {
                    return {
                        ...todo,
                        completed: !todo.completed
                    }
                }
                return todo
            })
            return {
                todos: newTodos
            }
        })
    }

    handleDelete = (id) => {
        this.setState(prevState => {
            const eraseTitle = prevState.todos.map(todo => {
                if (todo.id === id) {
                    return {
                        ...todo,
                        title: ''
                    }
                }
                return todo
            })
            return {
                todos: eraseTitle,
                itemCount: (this.state.itemCount > 0) ? prevState.itemCount - 1 : null
            }
        })
    }

    handleCompleted = () => {
        this.setState(prevState => {
            const eraseCompleted = prevState.todos.map(todo => {
                if (todo.completed) {
                    return {
                        ...todo,
                    title: ''
                    }
                }
                return todo
            })
            return {
                todos: eraseCompleted,
                itemCount: (this.state.itemCount > 0) ? prevState.itemCount - 1 : null
            }
        })
    }
     

      
    render() {
        const itemListing = this.state.todos.map((todo) => (
            <TodoItem key={todo.id} todo={todo} handleChange={this.handleChange} delete={this.handleDelete} />
          ))
      return (
        <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <form onSubmit={this.addItem}>
          <input className="new-todo"  
          onChange={this.handleInput} 
          placeholder="What needs to be done?" autoFocus />
        </form>
        </header>
        <section className="main">
          <ul className="todo-list">
            {itemListing}
          </ul>
        </section>
        <footer className="footer">
          <span className="todo-count">
        <strong>{this.state.itemCount}</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.handleCompleted}>Clear completed</button>
        </footer>
      </section>
      );
    }
  }

